﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour {
    static Dictionary<TeamColor, List<Character>> teamInfo = new Dictionary<TeamColor, List<Character>>();
    List<TeamColor> teamNames = new List<TeamColor>();
    public static bool nextTurn = false;
    public static List<Character> charQ;
    public static Character currentChar = null;
    internal Coroutine turnRotation;
    static TurnManager thisObject;
    static int victoryTeam = -1;


    public void SetCharacters()
    {
        thisObject = this;
        Character[] ch = FindObjectsOfType<Character>();
        teamInfo = new Dictionary<TeamColor, List<Character>>();
        foreach (Character c in ch)
        {
 
            if (!teamInfo.ContainsKey(c.team))
            {
                teamInfo[c.team] = new List<Character>();
            }
            if (c.playable)
            {
                teamInfo[c.team].Add(c);
            }
            
        }

        foreach (TeamColor st in teamInfo.Keys)
        {
            teamNames.Add(st);
        }

        turnRotation = StartCoroutine(TurnRotation());
    }
	
	IEnumerator TurnRotation()
    {
        int teamNum = 0;
        while (true)
        {
            StartTurn(teamNames[teamNum]);
            yield return new WaitUntil(() => nextTurn == true);
            teamNum = teamNum + 1 == teamNames.Count ? 0 : teamNum + 1;
        }
    }

    void StartTurn(TeamColor teamName)
    {
        charQ = new List<Character>();
        nextTurn = false;
        foreach(Character c in teamInfo[teamName])
        {
            charQ.Add(c);
            c.hasPlayed = false;
            c.isTurn = false;
            c.ResetSkill();
            
        }
        currentChar = charQ[0];
        charQ[0].StartTurn();
    }

    public static void EndTurn(Character ch)
    {
        int index = charQ.IndexOf(ch);
        ch.isTurn = false;
        ch.myTurn = false;
        ch.hasPlayed = true;
        charQ.Remove(ch);
        nextTurn = CheckEndTurn(ch.team);
        if (!nextTurn)
        {
        if (index >= charQ.Count)
            {
                index = 0;
            }
            currentChar = charQ[index];
            charQ[index].StartTurn();
 
        }
        CheckLandMines();
        Wall[] walls = FindObjectsOfType<Wall>();
        foreach (Wall w in walls)
        {
            if (w.hp <= 0)
            {
                w.TerminateWall();
            }
        }
    }

    private static void CheckLandMines()
    {
        LandMine[] landMines = FindObjectsOfType<LandMine>();
        foreach(LandMine lm in landMines)
        {
            if (lm.hasExploded)
            {
                lm.TerminateLandMine();
            }
        }
    }

    public static void ChangeCharacter(Character ch)
    {
        int index = charQ.IndexOf(ch) + 1;
        ch.isTurn = false;
        ch.myTurn = false;
        if (index >= charQ.Count)
        {
            index = 0;
        }
        currentChar = charQ[index];
        charQ[index].StartTurn();
    }

    static bool CheckEndTurn(TeamColor teamName)
    {
        int hasPlayedNum = 0;
        foreach(Character c in teamInfo[teamName])
        {
            if (c.hasPlayed)
            {
                hasPlayedNum++;
            }
        }
        if (hasPlayedNum >= teamInfo[teamName].Count)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public static bool HasEndedGame()
    {
        int survivingTeam = 0;
        foreach(TeamColor team in teamInfo.Keys)
        {
            if(teamInfo[team].Count > 0)
            {
                survivingTeam++;
                victoryTeam = (int)team;
            }
        }
        return survivingTeam <= 1 ? true : false;
    }

    public static void EndGame(int winningTeam)
    {
        FindObjectOfType<GameManager>().NextMatch(winningTeam);
        print("end game");
    }

    public static void CharacterDie(Character ch)
    {
        if(currentChar == ch)
        {
            EndTurn(ch);
        }
        charQ.Remove(ch);
        teamInfo[ch.team].Remove(ch);
        ch.GetComponent<SpriteRenderer>().enabled = false;
        ch.GetComponent<Collider2D>().enabled = false;
        if(ch.GetComponentInChildren<CharacterDirection>() != null)
        {
            ch.GetComponentInChildren<CharacterDirection>().GetComponent<SpriteRenderer>().enabled = false;
        }
        
        ch.dead = true;
        if (HasEndedGame())
        {
            thisObject.StopCoroutine(thisObject.turnRotation);
            EndGame(victoryTeam);
        }
    }


}

public enum TeamColor
{
    Red,
    Blue,
    Green,
    Yellow 
}

public static class TeamColorMethod
{
    public static Color GetColor(this TeamColor team)
    {
        switch (team)
        {
            case TeamColor.Red:
                return Color.red;
            case TeamColor.Blue:
                return Color.blue;
            case TeamColor.Green:
                return Color.green;
            case TeamColor.Yellow:
                return Color.yellow;
        }
        return Color.white;
    }
}