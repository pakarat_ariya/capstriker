﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpryteB : Character {
    public bool shield = false;

    protected override void Effect()
    {
        StartCoroutine(CreateShield());
    }

    IEnumerator CreateShield()
    {
        yield return new WaitUntil(() => rb.velocity.magnitude < .3f);
        if (hitOther)
        {
            shield = true;
        }
        StartCoroutine(DelayEndTurn());
    }

    public override void ResetSkill()
    {
        base.ResetSkill();
        shield = false;
    }

    public override void GetHit(float dmg)
    {
        float currentDamage = shield ? dmg / 2 : dmg;
        health -= currentDamage;
    }
}
