﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanbaG : Character {
    public Wall wallPref;
    internal Wall myWall = null;
    private int turnNum = 0;
    Vector3 wallPos;

    protected override void Effect()
    {
        StartCoroutine(CreateWall());
    }

    IEnumerator CreateWall()
    {
        yield return new WaitUntil(() => rb.velocity.magnitude <= .3f);
        foreach (Character c in FindObjectsOfType<Character>())
        {
           c.CheckHp();
        }
        float myAngle = transform.eulerAngles.z + 90;
        wallPos = transform.position + new Vector3(Mathf.Cos(Mathf.Deg2Rad * myAngle), Mathf.Sin(Mathf.Deg2Rad * myAngle), 0) * 2f;
        turnNum++;
        if (wallPref != null)
        {
            myWall = Instantiate<Wall>(wallPref);
            myWall.transform.position = wallPos;
        }
        isTurn = false;
        hasPlayed = false;
        StartCoroutine(DelayEndTurn());
    }
}
