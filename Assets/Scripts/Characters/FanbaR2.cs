﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanbaR2 : AuraCharacter {


    public override void GetAreaEffectStart(Character ch)
    {
        base.GetAreaEffectStart(ch);
        if(ch.team == team)
        {
            ch.currentChargeSpeed = ch.chargeSpeed * 0.6f;
            ch.currentMaxPower = ch.maxPower * 1.6f;
            ch.currentRotateSpeed = ch.rotateSpeed * 0.8f;
            ParticleSystem par = PoolManager.Spawn("ControlParticle");
            par.transform.position = ch.transform.position;
        }
    }

    
}
