﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanbaB2 : Character {

    public FanbaBElectric electricPref;
    internal FanbaBElectric myElectric = null;
    internal GameObject go = null;
    internal bool hasShot = false;
    internal float myAngle;

    protected override void Effect()
    {
        StartCoroutine(CreateElectric());

    }

    IEnumerator CreateElectric()
    {
        foreach (Character c in FindObjectsOfType<Character>())
        {
            c.CheckHp();
        }
        yield return new WaitUntil(() => rb.velocity.magnitude <= .3f);
        if (!dead)
        {
            hasPlayed = true;
            if (go == null)
            {
                go = new GameObject("go");
                go.transform.position = transform.position;
                go.transform.parent = transform;
            }
            if (myElectric == null)
            {
                myElectric = Instantiate<FanbaBElectric>(electricPref);
                myElectric.transform.parent = go.transform;
                myElectric.owner = this;
            }

            myElectric.gameObject.SetActive(true);
            myAngle = transform.eulerAngles.z + 45;
            float goalAngle = myAngle - 360;
            float rotateSpeed = 180;

            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
            go.SetActive(true);
            myElectric.ResetDamage();
            myElectric.transform.position = transform.position + new Vector3(Mathf.Cos(Mathf.Deg2Rad * myAngle) * 1.5f, Mathf.Sin(Mathf.Deg2Rad * myAngle) * 1.5f, 0);
            while (myAngle >= goalAngle)
            {
                go.transform.eulerAngles = new Vector3(0, 0, myAngle);
                myAngle -= rotateSpeed * Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            go.gameObject.SetActive(false);
            yield return new WaitForSeconds(.3f);
        }

        StartCoroutine(DelayEndTurn());
    }
}
