﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombMakerScript : Character {

    public LandMine bombPref;
    internal LandMine myBomb = null;
    private int turnNum = 0;
    Vector3 bombPos;

    protected override void Effect()
    {
        StartCoroutine(CreateBomb());
    }

    IEnumerator CreateBomb()
    {
        yield return new WaitUntil(() => rb.velocity.magnitude <= .3f);
        print(turnNum);
        if (turnNum == 0)
        {

            foreach (Character c in FindObjectsOfType<Character>())
            {
                c.CheckHp();
            }
            bombPos = transform.position;
            canChangeTurn = false;
            isTurn = true;
            turnNum++;

        }
        else if (turnNum == 1)
        {
            if (bombPref != null)
            {
                myBomb = Instantiate<LandMine>(bombPref);
                myBomb.transform.position = bombPos;
            }
            isTurn = false;
            hasPlayed = false;
            StartCoroutine(DelayEndTurn());
        }
    }

    public override void ResetSkill()
    {
        canChangeTurn = true;
        turnNum = 0;
    }
}
