﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Yo : Character {
    int playNum = 2;
    int playCount = 0;
    protected override void Effect()
    {
        if(playCount < playNum)
        {
            playCount++;
            canChangeTurn = false;
            isTurn = true;
            StartCoroutine(CheckCharactersDead());
        } else
        {
            StartCoroutine(DelayEndTurn());
        }
        
    }
    IEnumerator CheckCharactersDead()
    {
        yield return new WaitUntil(() => rb.velocity.magnitude < .3f);
        foreach (Character c in FindObjectsOfType<Character>())
        {
            c.CheckHp();
        }
    }

    public override void ResetSkill()
    {
        playCount = 0;
        canChangeTurn = true;
    }
}
