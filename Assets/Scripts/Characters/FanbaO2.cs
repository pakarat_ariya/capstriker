﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanbaO2 : Character {
    //int playNum = 1;
    int playCount = 0;

    public override void StartGame()
    {
        base.StartGame();
        currentChargeSpeed = chargeSpeed * 1.5f;
        currentMaxPower = maxPower * 1.5f;
        currentRotateSpeed = rotateSpeed * 1.5f;
    }

    protected override void Effect()
    {
        if (playCount == 0)
        {
            playCount++;
            canChangeTurn = false;
            isTurn = true;
            currentChargeSpeed = chargeSpeed;
            currentMaxPower = maxPower;
            currentRotateSpeed = rotateSpeed;
            StartCoroutine(CheckCharactersDead());
        } else
        {
            StartCoroutine(DelayEndTurn());
        }

    }
    IEnumerator CheckCharactersDead()
    {
        yield return new WaitUntil(() => rb.velocity.magnitude < .3f);
        foreach (Character c in FindObjectsOfType<Character>())
        {
            c.CheckHp();
        }
    }

    public override void ResetSkill()
    {
        playCount = 0;
        currentChargeSpeed = chargeSpeed * 1.5f;
        currentMaxPower = maxPower * 1.5f;
        currentRotateSpeed = rotateSpeed * 1.5f;
        canChangeTurn = true;
    }
}
