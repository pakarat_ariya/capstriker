﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpryteG : Character {
    public float radius = 5;
    public float pushPower = 5;

    protected override void Effect()
    {
        StartCoroutine(PushEveryOne());
    }

    IEnumerator PushEveryOne()
    {
        yield return new WaitUntil(() => rb.velocity.magnitude < 0.05f);
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, radius);
        foreach(Collider2D col in cols)
        {
            if(col.GetComponent<Character>() != null)
            {
                Character des = col.GetComponent<Character>();
                Vector3 dir = GetDirection(transform.position, des.transform.position).normalized;
                float distance = Vector3.Distance(transform.position, des.transform.position);
                des.rb.velocity = dir * (radius - distance);
            }
        }
        foreach (Collider2D col in cols)
        {
            if(col.GetComponent<Rigidbody2D>() != null)
            {
                yield return new WaitUntil(() => col.GetComponent<Rigidbody2D>().velocity.magnitude < 0.05f);
            }
        }
        StartCoroutine(DelayEndTurn());
    }

    private Vector3 GetDirection(Vector3 myPos, Vector3 desPos)
    {
        return (desPos - myPos) * pushPower;
    }


}
