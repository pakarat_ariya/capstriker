﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pepsee : Character {
    //static private bool created = false;
    protected override void Effect()
    {
        StartCoroutine(DelayEndTurn());
    }

    public override void StartGame()
    {
        base.StartGame();
        Vector3[] pos = { transform.position + new Vector3(-Mathf.Sign(transform.position.x) * 2, 0, 0)
                , transform.position + new Vector3(0, -Mathf.Sign(transform.position.y) * 2, 0) };
        for(int i = 0; i < 2; i++)
        {
            Pepsee myPs = Instantiate<Pepsee>(this);
            myPs.team = team;
            myPs.isPlayer = isPlayer;
            myPs.playingMode = true;
            if (!myPs.isPlayer)
            {
                myPs.gameObject.AddComponent<AiCharacter>();
            }
            myPs.transform.position = pos[i];
        }
        //created = true;
        
    }
}
