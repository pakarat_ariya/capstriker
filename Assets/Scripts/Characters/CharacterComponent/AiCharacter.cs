﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiCharacter : MonoBehaviour {

    Character myChar;
    Character currentTarget = null;
    List<Character> myEnemies = new List<Character>();
    float accThreshold = 30f;
    
    private void OnEnable()
    {
        
        myChar = GetComponent<Character>();
        
        
        StartCoroutine(AiDash());
    }

    IEnumerator AiDash()
    {
        yield return new WaitForSeconds(0.4f);
        foreach (Character ch in FindObjectsOfType<Character>())
        {
            if (ch.team != myChar.team)
            {
                myEnemies.Add(ch);
            }
        }
        while (true)
        {
            yield return new WaitUntil(() => (myChar.isTurn && !myChar.dead && myChar.rb.velocity.magnitude < .3f));
            
            
            float acc = Random.Range(-accThreshold, accThreshold);
            SelectEnemy();
            while (true)
            {
                Vector3 dToEnemy = currentTarget.transform.position - transform.position;
                float toTargetAngle = Mathf.Atan2(dToEnemy.y, dToEnemy.x) * Mathf.Rad2Deg;
                toTargetAngle = toTargetAngle + acc < 0 ? toTargetAngle + 360 + acc : toTargetAngle + acc;
                float angle = transform.eulerAngles.z + 90 > 360 ? transform.eulerAngles.z + 90 - 360 : transform.eulerAngles.z + 90;
                //print(this.name + ": angle = " + angle);
                //print(this.name + ": toTargetAngle = " + toTargetAngle);
               // Vector3 direction = new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
                if (Mathf.Abs(angle - toTargetAngle) <= 5)
                {
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
            
            myChar.charging = true;
            yield return new WaitUntil(() => (myChar.power >= 0.7f * myChar.maxPower));
            myChar.Release();
            myChar.charging = false;
            yield return new WaitForSeconds(1);
        }
    }

    private void SelectEnemy()
    {
        foreach (Character ch in myEnemies)
        {
            if (!ch.dead)
            {
                currentTarget = ch;
            }
        }
        foreach (Character ch in myEnemies)
        {            
            if (Vector3.Distance(transform.position, ch.transform.position) < Vector3.Distance(transform.position, currentTarget.transform.position) && !ch.dead)
            {

                currentTarget = ch;
            }
            
        }
    }
}
