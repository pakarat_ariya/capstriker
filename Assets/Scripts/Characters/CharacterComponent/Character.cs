﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class Character : MonoBehaviour {
    public TeamColor team = TeamColor.Red;
    public float maxHealth = 50;
    public float damage = 10;
    private CharacterDirection charD;
    float linearDrag = 5;
    public float health = 1;
    internal bool charging = false;
    internal bool dead = false;
    public float rotateSpeed = 300;
    public float maxPower = 50;
    internal float power = 0;
    public float chargeSpeed = 100;
    internal Rigidbody2D rb;
    public string charName;
    public float currentRotateSpeed;
    public float currentMaxPower;
    public float currentChargeSpeed;
    internal bool antiElectric = false;
    internal bool hitOther = false;
    internal bool getStartEffect = false;
    [TextArea]
    public string description = "";
    [TextArea]
    public string effect = "";
    public bool isPlayer;
    public bool isTurn = false;
    public bool hasPlayed = true;
    internal bool myTurn = false;
    internal bool canChangeTurn = true;
    internal bool canDrop = false;
    internal bool inHole = false;
    public bool playable = true;
    internal SpriteRenderer startSprite;
    internal Vector3 startPos;
    internal bool playingMode = false;
    internal HealthBar myHealthBar;
    internal ChargeBar myChargeBar;

	// ============================================== Mono Behaviour =============================================

	void Start () {
        myHealthBar = Instantiate<HealthBar>(Resources.Load<HealthBar>("Assistances/healthBar"));
        myHealthBar.InstantiateHealthBar(this);
        myHealthBar.hiding = true;
        myChargeBar = Instantiate<ChargeBar>(Resources.Load<ChargeBar>("Assistances/chargeBar"));
        myChargeBar.InstantiateChargeBar(this);
        myChargeBar.hiding = true;
        //cam = FindObjectOfType<Camera>();
        rb = GetComponent<Rigidbody2D>();
        charD = GetComponentInChildren<CharacterDirection>();
        health = maxHealth;
        rb.drag = linearDrag;
        rb.gravityScale = 0;
        rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb.sharedMaterial = Resources.Load<PhysicsMaterial2D>("PhysicMaterials/NormalMaterial");
        startSprite = GetComponent<SpriteRenderer>();
        currentChargeSpeed = chargeSpeed;
        currentMaxPower = maxPower;
        currentRotateSpeed = rotateSpeed;
        StartCoroutine(QuickShowHealthBar());
	}
	
	public virtual void Update () {
        if (dead)
        {
            return;
        }

        if (health <= 0)
        {
            startSprite.color = new Color(0.5f, 0.5f, 0.5f, 1);
        }

        if (charging)
        {
            StartCoroutine(QuickShowChargeBar());
            Charge();
        }
        if (isTurn)
        {
            if(GetComponent<AiCharacter>() == null)
            {
                ControlCharacter();
            }
            if (!charging)
            {
                Rotate();
            }
        }

        if(charD != null)
        {
            if (!hasPlayed)
            {
                charD.TurnOnColor(team);
            }
            else
            {
                charD.TurnOffColor();
            }
        }

        
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        StartCoroutine(QuickShowHealthBar());
        if (col.collider.GetComponent<Character>() && myTurn)
        {
            Character hitChar = col.collider.GetComponent<Character>();
            if (rb.velocity.magnitude + hitChar.rb.velocity.magnitude > 3)
            {
                hitChar.GetHit(damage);
                hitOther = true;
            }
            
        }
        if (col.collider.GetComponent<Character>() && !myTurn)
        {
            Character hitChar = col.collider.GetComponent<Character>();
            if (!hitChar.myTurn)
            {
                hitChar.GetHit(damage/2);
            }            
        }   
    }

    // ============================================== Character's Control ======================================

    void ControlCharacter()
    {

        if (Input.GetKeyDown(KeyCode.Space) && rb.velocity.magnitude < 1)
        {
            charging = true;
        }

        if (Input.GetKeyUp(KeyCode.Space) && charging)
        {
            charging = false;
            Release();
        }
        if (Input.GetKeyDown(KeyCode.Tab) && canChangeTurn)
        {
            NextCharacter();
        }
        
    }

    void Rotate()
    {
        transform.eulerAngles = transform.eulerAngles - Vector3.forward * Time.deltaTime * currentRotateSpeed;
    }

    internal void Charge()
    {
        if (power < currentMaxPower)
        {
            power += Time.deltaTime * currentChargeSpeed;
        } else
        {
            power = currentMaxPower;
        }
    }

    internal void Release()
    {
        Dash(power);
        power = 0;
        isTurn = false;
        Effect();
    }

    void Dash(float pow)
    {
        float angle = transform.eulerAngles.z + 90;
        Vector3 direction = new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
        rb.velocity = direction * pow;
    }

    // ================================================ Managing Turn =========================================

    public virtual void StartTurn()
    {
        ResetSkill();
        isTurn = true;
        myTurn = true;
        CheckAreaEffectStart();
    }


    public virtual void StartGame()
    {
        playingMode = true;
    }

    protected IEnumerator DelayEndTurn()
    {
        isTurn = false;
        yield return new WaitUntil(() => rb.velocity.magnitude < .3f);
        foreach (Character c in FindObjectsOfType<Character>())
        {
            yield return new WaitUntil(() => c.rb.velocity.magnitude < .3f);
        }
        yield return new WaitForSeconds(0.4f);
        foreach (Character c in FindObjectsOfType<Character>())
        {
            StartCoroutine(c.QuickShowHealthBar());
            c.CheckHp();
        }
        TurnManager.EndTurn(this);
    }

    IEnumerator DelayChangeTurn()
    {
        yield return new WaitForSeconds(0.2f);
        TurnManager.ChangeCharacter(this);
    }

    void NextCharacter()
    {
        StartCoroutine(DelayChangeTurn());
        foreach (Character c in FindObjectsOfType<Character>())
        {
            StartCoroutine(c.QuickShowHealthBar());
        }
    }

    // ============================================ Managing Character's stat =================================

    protected virtual void Effect()
    {
        StartCoroutine(DelayEndTurn());
    }

    public virtual void ResetSkill()
    {
        currentChargeSpeed = chargeSpeed;
        currentMaxPower = maxPower;
        currentRotateSpeed = rotateSpeed;
        getStartEffect = false;
        hitOther = false;
    }

    public void CheckAreaEffectStart()
    {
        if (!getStartEffect)
        {
            AuraCharacter[] allAura = FindObjectsOfType<AuraCharacter>();
            foreach (AuraCharacter ch in allAura)
            {
                if (ch.health > 0)
                {
                    Collider2D[] cols = Physics2D.OverlapCircleAll(ch.transform.position, ch.radius);
                    foreach (Collider2D col in cols)
                    {
                        if (col.GetComponent<Character>() == this)
                        {
                            ch.GetAreaEffectStart(this);
                        }

                    }
                }
            }
        }
        getStartEffect = true;
    }

    public virtual void CheckHp()
    {
        if (inHole)
        {
            if (!canDrop)
            {
                health = 0;
            }

        }
        if (health <= 0 && !dead)
        {
            myHealthBar.gameObject.SetActive(false);
            hasPlayed = true;
            isTurn = false;
            TurnManager.CharacterDie(this);
        }
    }

    public virtual void GetHit(float dmg)
    {
        StartCoroutine(DecreaseHp(health - dmg));
    }

    IEnumerator DecreaseHp(float result)
    {
        StartCoroutine(QuickShowHealthBar());
        while (health > result)
        {
            health -= 1;
            yield return new WaitForEndOfFrame();
        }
    }

    public virtual void Drop()
    {
        StartCoroutine(QuickShowHealthBar());
        if (!canDrop)
        {
            health = 0;
            CheckHp();
        }
    }

    // =================================================== Interface part =====================================

    public IEnumerator QuickShowHealthBar(float delay = 1.5f)
    {
        myHealthBar.hiding = false;
        yield return new WaitForSeconds(delay);
        myHealthBar.hiding = true;
    }

    public IEnumerator QuickShowChargeBar()
    {
        myChargeBar.hiding = false;
        yield return new WaitUntil(()=> charging == false);
        myChargeBar.hiding = true;
    }


}
