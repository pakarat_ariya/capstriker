﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDirection : MonoBehaviour {
    //Character myChar;
    SpriteRenderer sr;
    float timer = 0;
    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        //myChar = GetComponentInParent<Character>();
        sr.enabled = false;
    }

    public void TurnOnColor(TeamColor team)
    {
        if (sr != null)
        {
            sr.enabled = true;
            if(timer < .3f)
            {
                switch (team)
                {
                    case TeamColor.Blue:
                        sr.color = Color.blue;
                        break;
                    case TeamColor.Green:
                        sr.color = Color.green;
                        break;
                    case TeamColor.Red:
                        sr.color = Color.red;
                        break;
                    case TeamColor.Yellow:
                        sr.color = Color.yellow;
                        break;
                }
            } else
            {
                sr.color = Color.white;
            }
            if(timer >= .6f)
            {
                timer = 0;
            }
            timer += Time.deltaTime;
        }
        
    }

    public void TurnOffColor()
    {
        if (sr != null)
        {
            sr.enabled = false;
        }
        
    }

    
}
