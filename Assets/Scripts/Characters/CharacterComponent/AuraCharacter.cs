﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuraCharacter : Character {
    public float radius = 10;
    protected List<Character> charInArea;
    internal AreaEffect myEffect;
    public Particle myPar;
    public AreaEffect effectPref;
    private Coroutine areaRoutine;
    protected SpriteRenderer areaColor;

    public override void StartGame()
    {
        base.StartGame();
        myEffect = Instantiate<AreaEffect>(effectPref);
        myEffect.transform.position = transform.position;
        myEffect.transform.parent = transform;
        areaColor = myEffect.GetComponent<SpriteRenderer>();
        
    }

    public virtual void CheckAreaEffectEnd()
    {
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, radius);
        charInArea = new List<Character>();
        foreach (Collider2D col in cols)
        {
            if (col.GetComponent<Character>() != null)
            {
                charInArea.Add(col.GetComponent<Character>());
            }
        }
    }
    public virtual void GetAreaEffectStart(Character ch)
    {
        StartCoroutine(ShowAreaEffect());
    }

    public IEnumerator ShowAreaEffect()
    {
        while(areaColor.color.a < 1)
        {
            areaColor.color += new Color(0, 0, 0, 0.01f);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2);
        StartCoroutine(HideAreaEffect());
    }

    public IEnumerator HideAreaEffect()
    {
        while (areaColor.color.a > 0)
        {
            areaColor.color -= new Color(0, 0, 0, 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }
}
