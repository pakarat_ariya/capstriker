﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooterScript : Character {
    public FanbaB2Bullet bulletPref;
    internal FanbaB2Bullet myBullet = null;
    protected override void Effect()
    {
        StartCoroutine(CreateBullet());
        hasPlayed = true;
    }

    IEnumerator CreateBullet()
    {

        yield return new WaitUntil(() => rb.velocity.magnitude <= .3f);
        foreach (Character c in FindObjectsOfType<Character>())
        {
            c.CheckHp();
        }
        if (myBullet == null)
        {
            myBullet = Instantiate<FanbaB2Bullet>(bulletPref);
        }
        myTurn = false;
        myBullet.gameObject.SetActive(true);
        float myAngle = transform.eulerAngles.z + 90;
        myBullet.transform.position = transform.position + new Vector3(Mathf.Cos(Mathf.Deg2Rad * myAngle), Mathf.Sin(Mathf.Deg2Rad * myAngle), 0);
        myBullet.transform.eulerAngles = transform.eulerAngles;
        myBullet.isTurn = true;
        myBullet.canChangeTurn = false;
        myBullet.team = team;
        myBullet.hasPlayed = false;
        if (GetComponent<AiCharacter>() != null && myBullet.GetComponent<AiCharacter>() == null)
        {
            myBullet.gameObject.AddComponent<AiCharacter>();
        }
        myBullet.GetComponentInChildren<CharacterDirection>().TurnOnColor(team);
        yield return new WaitUntil(() => myBullet.isTurn == false);
        yield return new WaitUntil(() => myBullet.rb.velocity.magnitude <= .3f);
        myBullet.gameObject.SetActive(false);
        yield return new WaitForSeconds(.3f);
        StartCoroutine(DelayEndTurn());
    }
}
