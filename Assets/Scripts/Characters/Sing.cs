﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sing : Character {
    public SingHeart singHeartPref;
    public float singHp = 5;
    internal List<SingHeart> hearts = new List<SingHeart>();
    

    public override void StartGame()
    {
        base.StartGame();
        Vector3 dir = transform.position.normalized;
        transform.position -= dir * 2f;
        Vector3 myPos = transform.position;
            for(int j = -1; j <= 1; j++)
            {
                    myPos = transform.position;
                    myPos += new Vector3(dir.x * 3, j * 3 * dir.y, 0);
                    CreateMiniSing(myPos);
            }
            for (int j = 0; j <= 1; j++)
            {
                    myPos = transform.position;
                    myPos -= new Vector3(dir.x * 3 * j, -3 * dir.y, 0);
                    CreateMiniSing(myPos);
            }         
    }

    private void CreateMiniSing(Vector3 pos)
    {
        SingHeart miniSing = Instantiate<SingHeart>(singHeartPref);
        miniSing.transform.position = pos;
        miniSing.team = team;
        miniSing.owner = this;
    }

    public override void CheckHp()
    {
        if(singHp <= 0)
        {
            health = 0;
            TurnManager.CharacterDie(this);
            hasPlayed = true;
            isTurn = false;
        }
        if (inHole)
        {
            transform.position = startPos;
        }
    }
}
