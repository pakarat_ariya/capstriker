﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    internal Character owner;
    internal Color alpha = new Color(0, 0, 0, 0);
    private Color startingColor;
    internal bool hiding = false;
    Coroutine healthRoutine;

    private void Start()
    {
        if (owner.health <= 200 && owner.playingMode)
        {
            transform.position = owner.transform.position - new Vector3(owner.maxHealth / 50, 1, 0);
            if (owner.GetComponent<AiCharacter>() != null)
            {
                GetComponent<SpriteRenderer>().color = Color.gray - alpha;
            }
            else
            {
                GetComponent<SpriteRenderer>().color = owner.team.GetColor() - alpha;
            }
            startingColor = GetComponent<SpriteRenderer>().color;
            GetComponent<SpriteRenderer>().color = startingColor - new Color(0, 0, 0, 255);
        }
    }

    private void Update()
    {
        if ( owner.health <= 200 && owner.playingMode)
        {
            transform.localScale = new Vector3(owner.health / 10, 1, 1);
            transform.position = owner.transform.position - new Vector3(owner.maxHealth / 50, 1, 0);
        }
        else
        {
            GetComponent<SpriteRenderer>().enabled = false;

        }

        if(owner.health <= 0)
        {
            GetComponent<SpriteRenderer>().enabled = false;
        }else
        {
            GetComponent<SpriteRenderer>().color = startingColor + alpha;
        }

        if (hiding)
        {
            healthRoutine = StartCoroutine(HideHealthBar());
        }
        else
        {
            StartCoroutine(ShowHealthBar());
        }

        
    }

    public void InstantiateHealthBar(Character ch)
    {
        owner = ch;
    }

    public IEnumerator HideHealthBar()
    {

        
        while(alpha.a > -1)
        {
            alpha.a -= 0.001f;
            yield return new WaitForEndOfFrame();
        
        }
    }


    public IEnumerator ShowHealthBar()
    {
        if (healthRoutine != null)
        {
            StopCoroutine(healthRoutine);
        }
        while (alpha.a < 0)
        {
            alpha.a += 0.001f;
            yield return new WaitForEndOfFrame();
        }
    }
}
