﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class FanbaBElectric : MonoBehaviour {

    public float damage = 100;
    internal float currentDamage;
    private float dmgPerHit = 10;
    internal FanbaB2 owner = null;

    private void Start()
    {
        currentDamage = damage;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<Character>() != null)
        {
            Shot(other.GetComponent<Character>());
        }
        if(other.GetComponent<AntiElectricObject>() != null)
        {
            Shot(null);
        }
        if(other.GetComponent<LandMine>() != null)
        {
            currentDamage -= 100;
            CheckDamage();
        }
    }

    public void Shot(Character ch)
    {
        if (ch != null)
        {
            if (!ch.antiElectric)
            {
                ch.GetHit(dmgPerHit);
            }
        }        
        currentDamage -= dmgPerHit;
        owner.myAngle += 10;
        ParticleSystem par = PoolManager.Spawn("ElectricParticle");
        par.transform.position = transform.position;
        foreach (Character c in FindObjectsOfType<Character>())
        {
            c.CheckHp();
        }
        CheckDamage();
    }

    public void CheckDamage()
    {
        if(currentDamage <= 0)
        {
            gameObject.SetActive(false);
            owner.myAngle -= 360;
        }
    }

    public void ResetDamage()
    {
        currentDamage = damage;
    }
}
