﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class LandMine : MonoBehaviour {
    public bool hasExploded = false;
    public float bombDamage = 100;
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.GetComponent<Character>() != null)
        {
            Character ch = col.collider.GetComponent<Character>();
            ch.GetHit(bombDamage);
            hasExploded = true;
            GetComponent<SpriteRenderer>().color += new Color(0.5f, 0.5f, 0.5f);
        }
    }

    public void TerminateLandMine()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
    }
}
