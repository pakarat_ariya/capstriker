﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingHeart : Character {
    internal Sing owner;
    internal bool minusHp = false;
    public override void Update()
    {
        if (health <= 0 && minusHp == false)
        {
            minusHp = true;
            owner.singHp -= 1;
        }
    }

}
