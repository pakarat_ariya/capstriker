﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : AntiElectricObject {
    public float damage = 20;
    internal float hp = 100;
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.GetComponent<Character>() != null)
        {
            Character ch = col.collider.GetComponent<Character>();
            if (!ch.isTurn)
            {
                ch.GetHit(damage);
            }
            else
            {
                hp -= ch.damage;
            }
        }
        if (hp <= 0)
        {
            GetComponent<SpriteRenderer>().color += new Color(0.5f, 0.5f, 0.5f);
        }
    }

    public void TerminateWall()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
    }
}
