﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeBar : MonoBehaviour {

    internal Character owner;
    internal Color alpha = new Color(0, 0, 0, 0);
    private Color startingColor;
    internal bool hiding = false;
    Coroutine chargeRoutine;

    private void Start()
    {
        if (owner.playingMode)
        {
            transform.position = owner.transform.position + new Vector3(-owner.maxPower / 50, 1, 0);
            GetComponent<SpriteRenderer>().color = Color.yellow - alpha;
            startingColor = GetComponent<SpriteRenderer>().color;
            GetComponent<SpriteRenderer>().color = startingColor - new Color(0, 0, 0, 255);
        }
    }

    private void Update()
    {
        if (owner.playingMode)
        {
            transform.localScale = new Vector3(owner.power / 10, 1, 1);
            transform.position = owner.transform.position + new Vector3(-owner.maxPower / 50, 1, 0);
        }
        else
        {
            GetComponent<SpriteRenderer>().enabled = false;

        }

        if (owner.health <= 0)
        {
            GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = startingColor + alpha;
        }

        if (hiding)
        {
            chargeRoutine = StartCoroutine(HideChargeBar());
        }
        else
        {
            StartCoroutine(ShowChargeBar());
        }


    }

    public void InstantiateChargeBar(Character ch)
    {
        owner = ch;
    }

    public IEnumerator HideChargeBar()
    {


        while (alpha.a > -1)
        {
            alpha.a -= 0.001f;
            yield return new WaitForEndOfFrame();

        }
    }


    public IEnumerator ShowChargeBar()
    {
        if (chargeRoutine != null)
        {
            StopCoroutine(chargeRoutine);
        }
        while (alpha.a < 0)
        {
            alpha.a = 0;
            yield return new WaitForEndOfFrame();
        }
    }
}
