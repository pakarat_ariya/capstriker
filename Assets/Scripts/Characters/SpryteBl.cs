﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpryteBl : Character {

    public float radius = 5;
    public float cursePower = 5;
    public AreaEffect effectPref;
    internal AreaEffect myEffect;
    private SpriteRenderer areaColor;

    public override void StartGame()
    {
        base.StartGame();
        myEffect = Instantiate<AreaEffect>(effectPref);
        myEffect.transform.position = transform.position;
        myEffect.transform.parent = transform;
        areaColor = myEffect.GetComponent<SpriteRenderer>();
    }

    protected override void Effect()
    {
        StartCoroutine(CurseEveryone());
    }

    IEnumerator CurseEveryone()
    {
        yield return new WaitUntil(() => rb.velocity.magnitude < 0.05f);
        StartCoroutine(ShowAreaEffect());
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, radius);
        foreach (Collider2D col in cols)
        {
            if (col.GetComponent<Character>() != null)
            {
                Character des = col.GetComponent<Character>();
                if (team != des.team)
                {
                    Curse(des);
                }
            }
        }
        StartCoroutine(DelayEndTurn());
    }

    private void Curse(Character ch)
    {
        ch.health -= cursePower;
        ParticleSystem par = PoolManager.Spawn("CurseParticle");
        par.transform.position = ch.transform.position;
    }

    public IEnumerator ShowAreaEffect()
    {
        while (areaColor.color.a < 1)
        {
            areaColor.color += new Color(0, 0, 0, 0.03f);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2);
        StartCoroutine(HideAreaEffect());
    }

    public IEnumerator HideAreaEffect()
    {
        while (areaColor.color.a > 0)
        {
            areaColor.color -= new Color(0, 0, 0, 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }
}
