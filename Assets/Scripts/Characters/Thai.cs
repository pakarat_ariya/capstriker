﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thai : Character {
    public override void StartGame()
    {
        base.StartGame();
        canDrop = true;
    }

    public override void CheckHp()
    { 
        if (inHole)
        {
            health -= 10;
            transform.position = startPos;
        }
        base.CheckHp();
    }
}
