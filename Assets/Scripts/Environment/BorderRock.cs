﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class BorderRock : AntiElectricObject {
    public Sprite normalRock;
    public Sprite CrackingRock;
    SpriteRenderer sr;
    public int hp = 1;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        StartCoroutine(ChangePicture());
    }

    IEnumerator ChangePicture()
    {
        yield return new WaitUntil(() => hp == 1);
        sr.sprite = CrackingRock;
        yield return new WaitUntil(() => hp == 0);
        DestroyThis();
    }

    private void DestroyThis()
    {
        ParticleSystem par = PoolManager.Spawn("BreakingRockParticle");
        par.transform.position = transform.position;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.GetComponent<Character>() != null)
        {
            if(col.collider.GetComponent<VitawinkBullet>() != null)
            {
                Vector2 vel = col.relativeVelocity;
                DestroyThis();
                col.collider.GetComponent<VitawinkBullet>().rb.velocity = vel;
                return;
            }
            Character ch = col.collider.GetComponent<Character>();
            if(ch.rb.velocity.magnitude > 2)
            {
                hp--;
            }
        }
    }



}
