﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Hole : MonoBehaviour {

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Character>() != null)
        {
            col.GetComponent<Character>().inHole = true;
        }
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (col.GetComponent<Character>() != null)
        {
            col.GetComponent<Character>().inHole = false;
        }
    }
}
