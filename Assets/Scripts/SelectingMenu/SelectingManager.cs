﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectingManager : MonoBehaviour {

    Camera cam;
    public Dictionary<TeamColor, List<Character>> teamInfo = new Dictionary<TeamColor, List<Character>>();
    internal TeamColor selectingTeam;
    internal int teamNumber = 2;
    public List<Character> availableCharacter = new List<Character>();
    internal List<Character> chosenCharacter = new List<Character>();
    int memberPerTeam = 2;
    Character selectingCharacter = null;
    public SelectingMenuManager selectingMenu;

    private void Awake()
    {
        selectingMenu = FindObjectOfType<SelectingMenuManager>();

    }

    private void Start()
    {
        Character[] allCharacters = Resources.LoadAll<Character>("Characters");
        cam = FindObjectOfType<Camera>();
        selectingTeam = TeamColor.Red;
        memberPerTeam = FindObjectOfType<GameManager>().memberPerteam;
        int charIndex = 0;
        int col = 0;
        int row = 0;
        while (charIndex < allCharacters.Length)
        {
            Character ch = Instantiate<Character>(allCharacters[charIndex]);
            ch.name = allCharacters[charIndex].name;
            ch.transform.position = new Vector3(-3.5f + (1.5f * col), 7 - (1.5f * row), 0);
            col++;
            if(col >= 4)
            {
                col = 0;
                row++;
            }
            availableCharacter.Add(ch);
            charIndex++;
            
        }
    }

    void Update () {
        
        SelectingControl();
	}

    void SelectingControl()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hitInfo = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if(hitInfo.collider)
                if (hitInfo.collider.GetComponent<Character>() != null)
                {
                    ProcessCharacter(hitInfo.collider.GetComponent<Character>());
                    
                }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            SelectCharacter();
        }
  
    }

    public void ProcessCharacter(Character choosingCharacter)
    {
        if (selectingMenu != null)
        {
            selectingMenu.ChangeImage(choosingCharacter);
        }
        if (!chosenCharacter.Contains(choosingCharacter))
        {
            if (!teamInfo.ContainsKey(selectingTeam))
            {
                teamInfo[selectingTeam] = new List<Character>();
            }
            teamInfo[selectingTeam].Add(choosingCharacter);
            chosenCharacter.Add(choosingCharacter);
            
            choosingCharacter.GetComponent<SpriteRenderer>().color -= new Color(0.5f, 0.5f, 0.5f, 0);

            if(selectingCharacter != null && selectingCharacter != choosingCharacter )
            {
                RemoveCharacter(selectingCharacter);
            }
            
            selectingCharacter = choosingCharacter;

        }
        else if (teamInfo.ContainsKey(selectingTeam))
        {
            if (teamInfo[selectingTeam].Contains(choosingCharacter) && availableCharacter.Contains(choosingCharacter))
            {
                RemoveCharacter(choosingCharacter);
                selectingCharacter = null;
            }

        }
    }

    public void RemoveCharacter(Character choosingCharacter)
    {
        teamInfo[selectingTeam].Remove(choosingCharacter);
        chosenCharacter.Remove(choosingCharacter);
        choosingCharacter.GetComponent<SpriteRenderer>().color += new Color(0.5f, 0.5f, 0.5f, 0);
    }

    public void SelectCharacter()
    {
        // check if any character is being selected
        if(selectingCharacter != null)
        {
            // check if the number of player over the team number
            if ((int)selectingTeam + 1 < teamNumber)
            {
                selectingTeam = selectingTeam + 1;
            }
            else
            {
                // reset to the first team
                selectingTeam = TeamColor.Red;
            }
            availableCharacter.Remove(selectingCharacter);
            selectingCharacter = null;
        }
        List<bool> fullTeam = new List<bool>();
        for(int i = 0; i < teamNumber; i++)
        {
            fullTeam.Add(false);
        }
        int index = 0;
        // check if every player select all charaters
        foreach(TeamColor team in teamInfo.Keys)
        {
            if(teamInfo[team].Count == memberPerTeam)
            {
                fullTeam[index] = true;
            }
            index++;
        }
        if (!fullTeam.Contains(false))
        {
            //FindObjectOfType<GameManager>().SetTeam(teamInfo);
            MatchInformation matchInfo = new MatchInformation();
            foreach(TeamColor team in teamInfo.Keys)
            {
                TeamInformation myTeam = new TeamInformation();
                myTeam.isPlayer = true;
                foreach(Character ch in teamInfo[team])
                {
                    myTeam.members.Add(ch.name);
                }
                myTeam.winStatus = true;
                matchInfo.teams.Add(myTeam);
            }
            for (int i = matchInfo.teams.Count; i < GameManager.tournamentTeam; i++)
            {
                TeamInformation otherTeam = new TeamInformation();
                otherTeam.isPlayer = false;
                for (int j = 0; j < FindObjectOfType<GameManager>().memberPerteam; j++)
                {
                    // to protect ch from getting null reference.
                    Character ch = null;
                    while(ch == null)
                    {
                        int randomIndex = Random.Range(0, availableCharacter.Count);
                        ch = availableCharacter[randomIndex];
                        print(randomIndex + ": " + ch.name);
                        
                        availableCharacter.Remove(ch);
                        if(ch!= null)
                        {
                            otherTeam.members.Add(ch.name);
                        }
                        
                    }
                }
                otherTeam.winStatus = true;
                matchInfo.teams.Add(otherTeam);
            }
            matchInfo = ShuffleMatch(matchInfo);
            FindObjectOfType<GameManager>().SetTournamentMatch(matchInfo);
        }
    }

    public MatchInformation ShuffleMatch(MatchInformation matchInfo)
    {
        MatchInformation newMatchInfo = new MatchInformation();
        List<TeamInformation> teamList = new List<TeamInformation>();
        foreach(TeamInformation tInfo in matchInfo.teams)
        {
            teamList.Add(tInfo);
        }
        for(int i = 0; i < matchInfo.teams.Count; i++)
        {
            int randomIndex = Random.Range(0, teamList.Count);
            newMatchInfo.teams.Add(teamList[randomIndex]);
            teamList.RemoveAt(randomIndex);
        }
        return newMatchInfo;
    }
}
