﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectingMenuManager : MonoBehaviour {

    private Character selectedChar = null;
    public Image charImg;
    public Text charTxt;
    public Image charPanel;
    public Image teamMemberImg;
    public Image teamBgImg;
    internal Image[] teamColor;
    internal Image[] teamMembers;
    SelectingManager sm;

	// Use this for initialization
	void Start () {
        charImg.sprite = null;
        charTxt.text = "";
        sm = FindObjectOfType<SelectingManager>();
        teamColor = new Image[sm.teamNumber];
        for(int i = 0; i < teamColor.Length; i++)
        {
            Image img = Instantiate<Image>(teamBgImg);
            img.transform.parent = charPanel.transform;
            teamColor[i] = img;
            TeamColor myColor = (TeamColor)i;
            teamColor[i].color = myColor.GetColor();
            teamColor[i].name = myColor.ToString();
            float myWidth = Screen.width / (teamColor.Length);
            teamColor[i].rectTransform.sizeDelta = new Vector2(myWidth, 200);
            teamColor[i].rectTransform.Translate(myWidth * i + myWidth/2, 0, 0);
        }
	}
	
	public void ChangeImage(Character ch)
    {
        selectedChar = ch;
        charImg.sprite = ch.GetComponent<SpriteRenderer>().sprite;
        charTxt.text = selectedChar.charName +
            "\nhp: " + selectedChar.maxHealth +
            "\ndamage: " + selectedChar.damage +
            "\nspeed: " + selectedChar.maxPower +
            "\ncontrol: " + selectedChar.rotateSpeed +
            "\nspecial: " + selectedChar.effect;
    }

    
}
