﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public GameObject turnManagerPrefab;
    public TurnManager turnManager;
    public GameMode gameMode;
    public Dictionary<TeamColor, List<Character>> myTeamInfo = new Dictionary<TeamColor, List<Character>>();
    public int memberPerteam = 2;
    public Button startButton;
    public Button tournamentButton;
    public int tournamentTeamNum = 4;
    public static int tournamentTeam;
    public List<MatchInformation> matchInformation;
    public int tournamentRound = 0;
    public MatchInformation historyMatchInfo;
    public int currentMatch = 0;
    
    Vector3[] spawnPoints = { new Vector3(-6, -6, 0), new Vector3(-6, 6, 0), new Vector3(6, -6, 0), new Vector3(6, 6, 0) };
	
	void Start () {
        DontDestroyOnLoad(this);
        tournamentTeam = tournamentTeamNum;
        if (FindObjectOfType<GameManager>() != this)
        {
            Destroy(this.gameObject);
        }

	}
	
	public void SetTeam(Dictionary<TeamColor, List<Character>> teamInfo)
    {
        
        foreach(TeamColor team in teamInfo.Keys)
        {
            myTeamInfo[team] = new List<Character>();
            foreach(Character ch in teamInfo[team])
            {
                print(ch.name);
                myTeamInfo[team].Add(Resources.Load<Character>("Characters/" + ch.name));

            }
        }
        SceneManager.LoadScene(1);
        StartCoroutine(SetCharacterDelay());
        
    }

    IEnumerator SetCharacterDelay()
    {

        yield return new WaitUntil(() => SceneManager.GetActiveScene().buildIndex == 1);
        if (turnManagerPrefab)
        {
            GameObject go = Instantiate<GameObject>(turnManagerPrefab);
            turnManager = go.GetComponent<TurnManager>();

        }
        int teamIndex = 0;
        int characterIndex = 0;
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            Character ch = Instantiate<Character>(myTeamInfo[(TeamColor)(teamIndex)][characterIndex], spawnPoints[i], Quaternion.identity);
            ch.team = (TeamColor)(teamIndex);
            ch.StartGame();
            ch.startPos = ch.transform.position;
            if (!ch.isPlayer)
            {
                ch.gameObject.AddComponent<AiCharacter>();
            }
            if (characterIndex + 1 < memberPerteam)
            {
                characterIndex++;
            }
            else
            {
                teamIndex++;
                characterIndex = 0;
                if(teamIndex > myTeamInfo.Count)
                {
                    break;
                }
            }
            
        }
        yield return new WaitForSeconds(1f);
        if (turnManager)
        {
            turnManager.SetCharacters();
        }
        
    }

    public void SetTournamentMatch(MatchInformation matchInfo)
    {
        matchInformation = new List<MatchInformation>();
        matchInformation.Add(new MatchInformation());
        foreach (TeamInformation team in matchInfo.teams)
        {
            matchInformation[0].teams.Add(team);
        }
        SceneManager.LoadScene("TournamentScene");
        StartCoroutine(SetTournamentMatchDelay());
    }

    public void SetMatchInformation()
    {
        int lastIndex = matchInformation.Count - 1;
        if(currentMatch >= matchInformation[lastIndex].teams.Count)
        {
            currentMatch = 0;
            tournamentRound++;
        }
    }

    public void NextMatch(int winningTeam)
    {
        int lastIndex = matchInformation.Count - 1;
        int otherIndex = winningTeam == 0 ? 1 : 0;
        matchInformation[lastIndex].teams[currentMatch - 2 + winningTeam].winStatus = true;
        matchInformation[lastIndex].teams[currentMatch - 2 + otherIndex].winStatus = false;
        if (currentMatch >= matchInformation[lastIndex].teams.Count)
        {
            currentMatch = 0;
            tournamentRound++;
            print("end round one");
            CheckRound();
        }
        lastIndex = matchInformation.Count - 1;
        SceneManager.LoadScene("TournamentScene");
        StartCoroutine(SetTournamentMatchDelay());
        if (matchInformation[lastIndex].teams.Count == 1)
        {       
            Invoke("GoToStartScene", 5);   
        }        

    }

    public void GoToStartScene()
    {
        SceneManager.LoadScene("StartGame");
        Destroy(this.gameObject);
    }

    IEnumerator SetTournamentMatchDelay()
    {
        int lastIndex = matchInformation.Count - 1;
        yield return new WaitUntil(() => SceneManager.GetActiveScene().name == "TournamentScene");

        for(int i = 0; i < matchInformation.Count; i++)
        {
            ShowFirstRound(matchInformation[i], i);
        }
        if (matchInformation[lastIndex].teams.Count > 1)
        {
            yield return new WaitForSeconds(3);
            Dictionary<TeamColor, List<Character>> challengers = new Dictionary<TeamColor, List<Character>>();
            for (int i = 0; i < 2; i++)
            {
                challengers[(TeamColor)i] = new List<Character>();
                //print("matchInformation.teams[" + currentMatch + "].members");
                foreach (string memberName in matchInformation[lastIndex].teams[currentMatch].members)
                {
                    //print(memberName);
                    Character ch = Resources.Load<Character>("Characters/" + memberName);
                    if (!matchInformation[lastIndex].teams[currentMatch].isPlayer)
                    {
                        ch.isPlayer = false;
                    }
                    else
                    {
                        ch.isPlayer = true;
                    }
                    challengers[(TeamColor)i].Add(ch);
                    
                }
                currentMatch++;
            }
            SetTeam(challengers);
        }
    }

    private void ShowFirstRound(MatchInformation myMatchInfo, int round)
    {
        for (int i = 0; i < myMatchInfo.teams.Count; i++)
        {
            for (int col = 0; col < memberPerteam; col++)
            {
                Vector3 pos = new Vector3(-3.5f + (col * 1.1f) + (round * 3), 6 - (3 * round) - (3 * i) , 0);
                Character ch =Instantiate<Character>(Resources.Load<Character>("Characters/" + myMatchInfo.teams[i].members[col]), pos, Quaternion.identity);
                if (!myMatchInfo.teams[i].winStatus)
                {
                    ch.GetComponent<SpriteRenderer>().color -= new Color(0.5f, 0.5f, 0.5f, 0);
                }
            }
        }
    }

    private void CheckRound()
    {
        int lastIndex = matchInformation.Count - 1;
        if (tournamentRound >= matchInformation.Count)
        {
            matchInformation.Add(new MatchInformation());
        }
        int currentIndex = matchInformation.Count - 1;
        foreach(TeamInformation team in matchInformation[lastIndex].teams)
        {
            if (team.winStatus)
            {
                matchInformation[currentIndex].teams.Add(team);
            }
        }
    }
    public void StartGame()
    {
        startButton.gameObject.SetActive(false);
        tournamentButton.gameObject.SetActive(true);
        print("a");
    }

    public void PlayTournament()
    {
        SceneManager.LoadScene("SelectingScene");
    }

}

[Serializable]
public class TeamInformation
{
    public List<string> members = new List<string>();
    public bool winStatus = true;
    public bool isPlayer = false;
}

[Serializable]
public class MatchInformation
{
    public List<TeamInformation> teams = new List<TeamInformation>();
}

public enum GameMode
{
    Tournament
}
